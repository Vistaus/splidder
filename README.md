# Splider

Splid your bills with friends, even if they don't use Ubuntutouch


## License

### Code

Copyright (C) 2024  S60W79

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License version 3, as published by the
Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY
QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.

## Icons

All Icons are licensed unter the Creative commons License.

The creators of thoose Icons are:

* Delapouite
* Siegele Roland
* Lorc
* Stannered
* Remix Design

Some of the icons were mixxed by S60W79.
