��    N      �  k   �      �     �     �     �     �  	   �     �  
   �     �  
   �  =        B     I     R     b     r     x  
   �     �     �     �     �     �     �     �                    %  
   4     ?     Q     ]     i  
   n     y     �     �     �  	   �  	   �     �     �     �     �     �     �  
   	     	     	     :	  	   H	     R	     g	  	   	     �	     �	     �	  �   �	     t
     z
  	   �
     �
     �
     �
     �
  D   �
    +     D     I  
   O     Z  }   b     �  M   �     A     D  '   ^    �  	   �     �     �     �     �     �     �            V   0     �  	   �     �     �  
   �     �     �     �  $   �          .     D     [     d     |     �     �     �     �     �     �     �  	   �     �     �       
   '     2     R     ^     j     m     �     �     �     �  
   �     �  #   �     �  	   �     �  !     
   7  	   B     L     Z  �   r     !     (     0     9     Y  ,   a     �  `   �  J       X     ]  
   c  
   n  �   y       l   !     �     �  &   �     @       %   /      $                 H           1   =             4      ;   	                     (                           -       9   3       E   N                >      ,       +       <   M   *   2                7      J   #   :   G                 ?   
             K   C   '                  &   L          "       A   5       0   )       .      !   6   I       B       8      F   D    Abort About About the app Add Add Group Add Group Member Add Member Add Payment Add Person Add a group member. For multiple Members, separate Names by ; Amount Category Chooce Category Chooce Currency Close Commit to the project Consuments Consumer(s) Copy URL to clipboard Create new Group? Create new group Cross Platform Currency Distribute equavally Donate Easy to use Failed Group Settings Group name Internal payment: Invite Code Invite code Join Join group Join or create group Loading Members Mismatch in the distribution New Group New group OK Official Website Payed by Payer(s) Payers Payment Payment to Person's name Please adjust group settings Please choose Quellcode Rates in relatio to  Rates in relatio to USD Recipient Refresh Report bugs Safe Settings See on one view the saldo (in green) and Absolute Payments of each group Member.
Further there are Icons for the payment categories and some other features, missing in the original, closed-source app. Share Spending Spendings Splid Data Protection Splider Swipe to join or create group Thanks for choosing Splider! This will create an empty group. You can do all settings afterwards. Thought there are many features to be implemented: Search filters, CSV Export, further overview, translations, ...
Also reverse engineering this awfull API alone was cruel.
So I'd be really, really happy about support, wether as a commitment to this project or a small donation (: Time Title Version %1 Welcome With this app you can share and split the bills cross platform. The Splid app uses the same API and works an IOS and Android. Yet so much to do! You are not logged in any groups yet.
Swipe upwards to join or create a group by https://splid.app/english https://splid.app/english/privacypolicy Project-Id-Version: splider.s60w79
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-02-17 16:39+0100
Last-Translator: S60W79
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.4.2
 Abbrechen Über Über diese App Hinzufügen Gruppe hinzu fügen Gruppenmitglied hinzu fügen Mitglied hinzu fügen Zahlung hinzu fügen Person hinzufügen Gruppenmitglied hinzufügen. Mit Semikolion ( ; ) trennen, um mehrere hinzu zu fügen. Betrag Katagorie Kategorie auswählen Währung auswählen Schließen Am Projekt beteiligen Nutzer Nutzende URL in den Zwischenspeicher kopieren Neue Gruppe erstellen? Neue Gruppe erstellen Plattformübergreifend Währung Gleichmäßig aufteilen Spenden Intuitiv Fehler Gruppeneinstellungen Gruppenname Interne Zahlung Einladungscode Einladungscode Beitreten Gruppe beitreten Gruppe beitreten oder erstellen Laden... Mitglieder Unstimmigkeit in der Verteilung Neue Gruppe Neue Gruppe OK Ofizielle Webseite Gezahlt von Zahlende Zahler Zahlung Zahlung an Name der Person Bitte Gruppeneinstellungen anpassen Bitte auswählen Quellcode Kurs im Verhältnis zu Kurs im Verhältnis zum US Dollar Empfänger Neu laden Fehler melden Einstellungen speichern Du kannst auf einem Blick den Saldo und die Gesamtausgaben einer Person sehen.
Außerdem gibt es Icons und einige Features, die in der eigentlichen, properitären App fehlen. Teilen Ausgabe Ausgaben Datenschutzerklärung von Splid Splider Nach oben wischen oder eine Gruppe erstellen Danke, dass du Splider nutzt! Dies wird eine leere Gruppe erstellen. Du kannst alle sonstigen Einstellungen später vornehmen. Trotzdem gibt es noch einiges, was zu implementieren wäre: Suchfilter, CSV Exporte, Mehr Statistiken, Übersetzungen, ...
Außerdem war es nicht schön, diese furchtbare Api zu reverse-engineeren.
Deshalb wäre ich für Unterstützung wirklich sehr dankbar, egal ob das eine Beteiligung am Projekt, oder eine kleine Spende ist (: Zeit Titel Version %1 Willkommen Mit dieser App kannst du Rechnungen teilen und das auch plattformübergreifend.
Splid (verfügbar unter Android und IOS) nutzt die selbe API. Es gibt noch viel zu tun! Du bist bisher in keiner Gruppe Mitglied.
Wische nach oben um dich anzumelden oder eine Gruppe zu erstellen. von https://splid.app/german https://splid.app/german/privacypolicy 