��    N      �  k   �      �     �     �     �     �  	   �     �  
   �     �  
   �  =        B     I     R     b     r     x  
   �     �     �     �     �     �     �     �                    %  
   4     ?     Q     ]     i  
   n     y     �     �     �  	   �  	   �     �     �     �     �     �     �  
   	     	     	     :	  	   H	     R	     g	  	   	     �	     �	     �	  �   �	     t
     z
  	   �
     �
     �
     �
     �
  D   �
    +     D     I  
   O     Z  }   b     �  M   �     A     D  '   ^  -  �     �     �     �  	   �     �     �     �            E   ,     r  	   y     �     �     �     �     �     �     �  %   �          "     9     @     P     X     o     w  
   �     �     �     �  
   �  
   �     �     �       #        6     C     P     U     h  
   u     �     �     �     �     �     �  	   �     �     �  	              )     5  �   J     #  
   )     4     =     Q  '   Y     �  I   �    �     �       	          �        �  V   �            '   8     @       %   /      $                 H           1   =             4      ;   	                     (                           -       9   3       E   N                >      ,       +       <   M   *   2                7      J   #   :   G                 ?   
             K   C   '                  &   L          "       A   5       0   )       .      !   6   I       B       8      F   D    Abort About About the app Add Add Group Add Group Member Add Member Add Payment Add Person Add a group member. For multiple Members, separate Names by ; Amount Category Chooce Category Chooce Currency Close Commit to the project Consuments Consumer(s) Copy URL to clipboard Create new Group? Create new group Cross Platform Currency Distribute equavally Donate Easy to use Failed Group Settings Group name Internal payment: Invite Code Invite code Join Join group Join or create group Loading Members Mismatch in the distribution New Group New group OK Official Website Payed by Payer(s) Payers Payment Payment to Person's name Please adjust group settings Please choose Quellcode Rates in relatio to  Rates in relatio to USD Recipient Refresh Report bugs Safe Settings See on one view the saldo (in green) and Absolute Payments of each group Member.
Further there are Icons for the payment categories and some other features, missing in the original, closed-source app. Share Spending Spendings Splid Data Protection Splider Swipe to join or create group Thanks for choosing Splider! This will create an empty group. You can do all settings afterwards. Thought there are many features to be implemented: Search filters, CSV Export, further overview, translations, ...
Also reverse engineering this awfull API alone was cruel.
So I'd be really, really happy about support, wether as a commitment to this project or a small donation (: Time Title Version %1 Welcome With this app you can share and split the bills cross platform. The Splid app uses the same API and works an IOS and Android. Yet so much to do! You are not logged in any groups yet.
Swipe upwards to join or create a group by https://splid.app/english https://splid.app/english/privacypolicy Project-Id-Version: splider.s60w79
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-02-19 13:19+0100
Last-Translator: Heimen Stoffels <vistausss@fastmail.com>
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.4.2
 Afbreken Over Over de app Toevoegen Groep samenstellen Lid toevoegen Lid toevoegen Betaling toevoegen Persoon toevoegen Voeg een groepslid toe. Scheid meerdere personen met behulp van een ; Bedrag Categorie Categorie kiezen Valuta kiezen Sluiten Doneren Aantal consumpties Verbruiker(s) Url kopiëren naar klembord Wil je een nieuwe groep samenstellen? Nieuwe groep Platform-onafhankelijk Valuta Gelijk verdelen Doneren Eenvoudig te gebruiken Mislukt Groepsinstellingen Groepsnaam Interne betaling: Uitnodigingscode Uitnodigingscode Lid worden Lid worden Groep maken of lid worden Bezig met laden… Leden Het bedrag is niet eerlijk verdeeld Nieuwe groep Nieuwe groep Oké Officiële website Betaald door Betaler(s) Betalers Betaling Betaling aan Naam van persoon Pas de groepsinstellingen aan Kiezen Quellcode Waarden ten opzichte van  Waarden ten opzichte van USD Ontvanger Herladen Bugs melden Veilige instellingen Aan de ene kant zie je het saldo (in het groen) en aan de andere kant de betalingen van ieder groepslid.
Verder zijn er pictogrammen voor betaalcategorieën en andere functies die in de oorspronkelijke app ontbreken. Delen Uitgegeven Uitgaven Gegevensbescherming Splider Veeg om groep te maken of lid te worden Welkom in Splider! Hiermee maak je een lege groep aan. Nadien kun je alles in- en afstellen. Er staan nog een hoop functies op het lijstje, waaronder zoekfilters, exporteren naar csv, meer overzichten en vertalingen.
Uitzoeken hoe de api werkt was al vreselijk genoeg.
Ik zou het daarom zeer op prijs stellen als je een kleine donatie zou doen, hetzij eenmalig, hetzij vaker. (: Tijdstip Titel Versie %1 Welkom Met deze app kun je rekeningen delen en splitsen, ongeacht het besturingssysteem. Er wordt gebruikgemaakt van dezelfde api als op iOS en Android. Nog zoveel te doen! Je bent nog geen lid van een groep.
Veeg vanaf de onderkant omhoog om acties te tonen. door https://splid.app/english https://splid.app/english/privacypolicy 