import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Lomiri.Components.Popups 1.3
import "libs/groups.js" as Groups
Dialog {

    id: paymentDia
    title: i18n.tr(model.name)
    text: model.category
    Label {
        text: model.price+" "+model.cur
        horizontalAlignment:Text.AlignHCenter
        textSize: Label.XLarge
    }
    Label{
        text:i18n.tr("Payed by")+" "+model.payer
    }
    Label{
        text:i18n.tr("Time")+" "+model.date
    }
        Label{
        text:i18n.tr("Consuments")
        horizontalAlignment:Text.AlignHCenter
        font.bold:true
    }
    Flickable{
    width:parent.width
    height:units.gu(9)*personModel.count
    PersonList{
        id:consuments

        model: ListModel {
            id:personModel
        }
    }
    }
    Label{
        visible:payerModel.count>1
        text:i18n.tr("Payers")
        horizontalAlignment:Text.AlignHCenter
        font.bold:true
    }

    Flickable{
        visible:payerModel.count>1
    width:parent.width
    height:units.gu(10)*payerModel.count
    PersonList{
        id:payersLst

        model: ListModel {
            id:payerModel
        }
    }
    }
    Button {
        id:cloBu
        text: i18n.tr("Close")
        onClicked: PopupUtils.close(paymentDia)
    }
    Component.onCompleted:{
        //fill consumers list
            Object.keys(model.balance["P"]["P"]).forEach(function(consument){
            const share = model.balance["P"]["P"][consument];
            personModel.append({name:Groups.resolveName(consument), spendings:true,percent:Math.round(share*100), absolute:Math.round(share*model.balance["AM"]*100)/100+model.cur})
        })
        if(secondary != {}){
            console.log("generate lst...")
            //fill payers list if there are secondary payers.
            var rest = model.balance["AM"]
            Object.keys(model.secondary).forEach(function(secPayer){
                const amt = model.secondary[secPayer];
                console.log("generate lst...", amt)
                payerModel.append({name:Groups.resolveName(secPayer), spendings:true,percent:Math.round(amt/model.balance["AM"]*100), absolute:Math.round(amt*100)/100+model.cur})
                rest -=amt
            })
            payerModel.append({name:Groups.resolveName(model.primary), spendings:true ,percent:Math.round(rest/model.balance["AM"]*100), absolute:Math.round(rest*100)/100+model.cur})
    }

    }
}
