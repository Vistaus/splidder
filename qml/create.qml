import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Lomiri.Components.Popups 1.3
import "libs/login.js" as Login
import Qt.labs.settings 1.0

Page {
    id:joinPage
    width: bottomEdge.width
    height: mainPage.height
    header: PageHeader {
        id:scanHead
        title: i18n.tr("Add Group")
    }
property int originalHeight : 0
    FailDia{
        id:fail
    }


    property string latestMessage : ""
    Flickable{
        anchors {
        top: scanHead.bottom
        left: parent.left
        right: parent.right
        bottom: parent.bottom
        }
        width:parent.width
        Column{
            id:cols
            anchors.fill:parent
            spacing:1


            width:parent.width
        Row{
            spacing:1
            Label{
                text:i18n.tr("Create new Group?")
            }

        Switch {
          id: createNotJoin
        }
        }
        TextField{
            id:code
            visible:!createNotJoin.checked
            placeholderText:i18n.tr("Invite code")
            width:parent.width
            onAccepted:Login.joinGroup()
        }
        


        Button{
            iconName:"ok"
            color:LomiriColors.green
            width:parent.width
            text:createNotJoin.checked?i18n.tr("Create new group"):i18n.tr("Join group")
            onClicked:add()

        }
        Label{
            width:parent.width
            wrapMode:Label.WordWrap
            visible:createNotJoin.checked
            textSize: Label.Small
            text:i18n.tr("This will create an empty group. You can do all settings afterwards.")
        }
        }
    }
function add(){
    if(createNotJoin.checked){
        Login.createGroup()
    }else{
        Login.joinGroup()
    }

}
}

