import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Lomiri.Components.Popups 1.3
import "libs/groups.js" as Groups
import "libs/delete.js" as Delete

Page {
    id:groupPage
    property var currentCur : ""
    property var groupinfo : {}
    property var payments : []
    property var members : []
    property var saldo : {}
    property var cat2icon : {"transport":"train", "accommodation":"house", "groceries":"groceries", "entertainment":"party", "restaurants":"restaurant", "transaction":"transfer"}
    property string invite:"???"
    property double allSum : 0.00
    Component {
        id:plusPersonDia
        AddPerson{

        }
    }
    Component {
        id:plusPayDia
        TransForm{

        }
    }
    Component.onCompleted: loadGroup();
    header: PageHeader {
        id:groupHead
        title: i18n.tr("Loading")
        extension: Sections {
            id: listSwitch

            anchors {
                left: parent.left
                bottom: parent.bottom
            }

            StyleHints {
                selectedSectionColor: LomiriColors.blue
            }

            model: [i18n.tr("Spendings"), i18n.tr("Members"), i18n.tr("Share")]
            // onSelectedIndexChanged: {
            //     refreshModel();
            // }
        }
        trailingActionBar.actions: [
                Action {
                    iconName: "add"
                    text: i18n.tr('Add Member')
                    visible:listSwitch.selectedIndex==1
                    onTriggered:PopupUtils.open(plusPersonDia)
                },
                Action {
                    iconName: "add"
                    text: i18n.tr('Add Member')
                    visible:listSwitch.selectedIndex==0
                    onTriggered:PopupUtils.open(plusPayDia)
                },
                Action{
                    iconName:"sync"
                    text: i18n.tr('Refresh')
                    onTriggered:{
                        refresh()

                    }
                },
                Action {
                    iconName: "settings"
                    text: i18n.tr('Group Settings')
                    //open settings and reach title, groupid and the current currency.
                    onTriggered:{
                        console.log("CUR", currentCur);
                        mainPage.pageStack.addPageToNextColumn(mainPage, Qt.resolvedUrl("groupSettings.qml"), {preGroupName:groupHead.title, groupId:root.currentGroup, currency:groupinfo.defaultCurrencyCode});
                    }
                }
            ]
    }
    Column{
        id:overwiewCol
        anchors {
            top: groupHead.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        spacing:units.gu(1)
        visible:listSwitch.selectedIndex==2

        Label{
            width:parent.width
            horizontalAlignment:Text.AlignHCenter
            font.bold:true
            text:i18n.tr("Invite Code")
        }
        Label{
            width:parent.width
            horizontalAlignment:Text.AlignHCenter
            text:invite
            textSize:Label.XLarge
        }

        QRCode{

            value:"https://splid.net/j/"+invite
            width:Math.min(parent.width/2, parent.height/2)
            height:Math.min(parent.width/2, parent.height/2)
            x:(parent.width-this.width)/2
        }
        Label{
            width:parent.width
            horizontalAlignment:Text.AlignHCenter
            text:"https://splid.net/j/"+invite
            font.bold:true
            textSize:Label.Small
        }
        Button{
            text:i18n.tr("Copy URL to clipboard")
            iconName:'copy'
            width:parent.width
            onClicked:Clipboard.push("https://splid.net/j/"+invite)
        }
    }
    PersonList{
        visible:listSwitch.selectedIndex==1
        //members with their saldo (index 1)
        anchors {
            top:groupHead.bottom
            left:parent.left
            right:parent.right
            bottom:parent.bottom
        }
        model: ListModel {
            id:memberModel
        }
    }
      ListView {
          //spendings list (index 0)
            anchors {
                top: groupHead.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            width: parent.width
            height: parent.height
            visible:listSwitch.selectedIndex==0
            model: ListModel {
                id:paymentsModel
            }

             delegate: Component{
                 Column{
            width:parent.width
            spacing:1
                 ListItem {
                     id:payIt
                    Component {
                        id: dia
                        PaymentDia{

                        }
                    }
                    leadingActions: ListItemActions {
                            actions: [
                                Action {
                                    iconName: "delete"
                                    onTriggered:{
                                        Delete.payment(model.id, function(status, txt){
                                           console.log("DELETE",status, txt)
                                           payIt.destroy()
                                        });
                                    }
                                }
                            ]
                        }
                    ListItemLayout {
                        id:paymentsLayout
                        title.text: model.name
                        subtitle.text: model.price+" "+model.cur+" "+i18n.tr("by")+" "+model.payer
                        Icon {
                            source:model.category in cat2icon?Qt.resolvedUrl("../assets/"+cat2icon[model.category]+".png"):Qt.resolvedUrl("../assets/unknown.png")
                            SlotsLayout.position: SlotsLayout.Leading;
                            //width: units.gu(2)
                            //height:parent.height/2
                            //width:parent.height/2
                            height:paymentsLayout.title.height+paymentsLayout.subtitle.height
                            width:paymentsLayout.title.height+paymentsLayout.subtitle.height
                        }
                    }
                    onClicked:PopupUtils.open(dia)

                    }

            }
        }
      }
BottomEdge {
    id: bottomEdge
    height: parent.height*0.2
    //hint.text: "progression"
    contentComponent: Column {
        PageHeader{
            id:"bEdge"
            title:i18n.tr("Add")
        }
        width: bottomEdge.width
        height: bottomEdge.height
        Button{
            height:(parent.height-bEdge.height)*0.49
            width:parent.width
            iconName:"compose"
            text:i18n.tr("Add Payment")
            onClicked:{
                PopupUtils.open(plusPayDia)
                bottomEdge.collapse()
            }
        }
        Button{
            height:(parent.height-bEdge.height)*0.49
            width:parent.width
            iconName:"contact-new"
            text:i18n.tr("Add Group Member")
            onClicked:{
                PopupUtils.open(plusPersonDia)
                bottomEdge.collapse()
            }
        }
    }
}


      function loadGroup(){
              //on Page loaded: load group members and group spendings

    root.groupInfos.forEach(function(info){
        //find out name from cach and show them in the header
        if(!("group" in info))return;
        if(info["group"]["objectId"] == root.currentGroup){
            groupinfo = info;
            groupHead.title=info["name"]
            currentCur = info["defaultCurrencyCode"]
            console.log("CUR:", currentCur)
    }
    //find invite code
    sets.loginData.forEach(function(keyset){
        JSON.stringify(keyset)
        console.log(JSON.stringify(keyset))
        if(keyset["objectId"]==root.currentGroup){
            if("code" in keyset)invite=keyset["code"]
            if("shortCode" in keyset)invite=keyset["shortCode"]
        }
    })
})
root.lowSaldo = 100000000000
root.highSaldo = -10000000000
Groups.members(root.currentGroup, "1969-12-31T00:00:00.000Z", 0, 100, function(status, personData){
    //load members
    //request group Members
    if(status){
        //on success, load spendings.
        groupPage.members = personData["result"]["results"];
        Groups.payments(root.currentGroup, "1969-12-31T00:00:00.000Z", 0, 100, function(status, payData){
            //request group Payments
            if (status){
                groupPage.payments = payData["result"]["results"]
                groupPage.payments.forEach(function(payment){
                    //skip if is deleted element!
                    if(payment["isDeleted"])return;
                    var title = "-";
                    var cat = i18n.tr("Spending")
                    const pricing = payment["items"][0]
                    allSum += pricing["AM"];
                    if("title" in payment)title=payment["title"]
                    if("category" in payment)cat=payment["category"]["originalName"]

                    if(payment["isPayment"]){
                        //is payment from one group member to another
                        cat = i18n.tr("Payment")
                        console.log("PRICING",JSON.stringify(pricing), JSON.stringify(pricing["P"]["P"]))
                        title = i18n.tr("Payment to")+" "+Groups.resolveName(Object.keys(pricing["P"]["P"])[0])
                    }
                    var payers = Groups.resolveName(payment["primaryPayer"])
                    var secondaryPayers = {}
                    if("secondaryPayers" in payment){
                        secondaryPayers = payment["secondaryPayers"]
                        Object.keys(payment.secondaryPayers).forEach(function(sPayer){
                            payers+=", "+Groups.resolveName(sPayer)
                        })
                    }
                    paymentsModel.append({name:title, price:pricing["AM"], cur:payment["currencyCode"], id:payment["objectId"], payer:payers, balance:pricing, date:payment["createdAt"], category:cat, primary:payment["primaryPayer"], secondary:secondaryPayers, category:"category" in payment?payment["category"]["type"]:payment["isPayment"]?"transaction":"unknown"})
                })
                 //complete persona list
                var noMembers = true
                    groupPage.members.forEach(function(member){
                        console.log("test", member["name"])
                        const paid = Groups.sumPayments(member.GlobalId)
                        const sal = Math.round((paid-Groups.consumtions(member.GlobalId))*100)/100
                        if(sal > root.highSaldo)root.highSaldo = sal
                        if(sal < root.lowSaldo)root.lowSaldo = sal
                        memberModel.append({name:member["name"], percent:0, spendings:false, saldo:sal, absolute:Math.round(paid*100)/100,curency:groupinfo.defaultCurrencyCode})
                        noMembers = false
                    })
                    //if no members yet, open dialog
                    if(noMembers)PopupUtils.open(plusPersonDia)
                    console.log("HIGH", root.highSaldo, "LOW", root.lowSaldo)
            }
            });
    }
});
    }
    function refresh(){
        //delete list contents and load group
        memberModel.clear()
        paymentsModel.clear()
        loadGroup();
    }
}
    
