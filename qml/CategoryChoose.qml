import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Lomiri.Components.Popups 1.3


         Dialog {

             id: regDia
             title: i18n.tr("Chooce Category")

            Flickable{
                                width:parent.width
                height:units.gu(30)
                        ListView {
            id:groupLst
            width: parent.width
            height: parent.height
            //visible:sets.loginData.length !=0

                model: ListModel {
                    id:catModel
                    ListElement {name:"groceries"; identifier:"groceries"}
                    ListElement {name:"transport"; identifier:"transport"}
                    ListElement {name:"accommodation"; identifier:"accommodation"}
                    ListElement {name:"entertainment"; identifier:"entertainment"}
                    ListElement {name:"restaurants"; identifier:"restaurants"}
                    ListElement {name:"unspecified"; identifier:"unspecified"}
                }

                delegate: Component{
                    Column{
                width:parent.width
                spacing:1
                    ListItem {

                        ListItemLayout {
                            id:catLayout
                            title.text: model.name
                            Icon {
                            source:model.identifier in cat2icon?Qt.resolvedUrl("../assets/"+cat2icon[model.identifier]+".png"):Qt.resolvedUrl("../assets/unknown.png")
                            SlotsLayout.position: SlotsLayout.Leading;
                            height:catLayout.title.height+catLayout.subtitle.height
                            width:catLayout.title.height+catLayout.subtitle.height
                        }
                        }
                        onClicked:{
                            category = model.identifier
                             PopupUtils.close(regDia)
                        }
                        }

                }
            }

            }
        }
         }

