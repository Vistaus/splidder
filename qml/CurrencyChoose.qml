import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Lomiri.Components.Popups 1.3


         Dialog {

             id: regDia
             title: i18n.tr("Chooce Currency")

            Flickable{
                                width:parent.width
                height:units.gu(30)
                        ListView {
            id:groupLst
            width: parent.width
            height: parent.height
            //visible:sets.loginData.length !=0

                model: ListModel {
                    id:curModel
                    //{name:"test"}
                }

                delegate: Component{
                    Column{
                width:parent.width
                spacing:1
                    ListItem {

                        ListItemLayout {
                            title.text: model.name
                            subtitle.text: rate
                        }
                        onClicked:{
                            currency = model.name
                            PopupUtils.close(regDia)
                        }
                        }

                }
            }

            }
        }
        Component.onCompleted: {
                //
                var eins = ""
                Object.keys(available).forEach(function(currency){
                   curModel.append({name:currency, rate:available[currency]})
                   if(available[currency]==1)eins = currency
                });
                if(available["USD"] == 1){
                    regDia.text = i18n.tr("Rates in relatio to USD")
                }else{
                    regDia.text = i18n.tr("Rates in relatio to ")+eins
                }
            }
         }

