import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Lomiri.Components.Popups 1.3

Component {
         Dialog {
             
             id: regDia
             title: i18n.tr("Failed")
             text: latestMessage
             Button {
                 text: i18n.tr("Close")
                 color: LomiriColors.red
                 onClicked: PopupUtils.close(regDia)
             }
         }
    }
