import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

ListView {
    id:viewer
    //visible:sets.loginData.length !=0
height:units.gu(15)
width:parent.width
//if payment, only this index is to be checked
property var checkedBu : []
property var distribution : {}
property var primary : ""
property bool absoluteLock:false;
property bool relativeLock:false;
delegate:Item {
        width: parent.width
        height: units.gu(8) // Adjust the height as needed

        Item {
            width: parent.width
            height: parent.height


            Row {
                width:parent.width
                anchors.fill: parent
                spacing: units.gu(2)
                CheckBox {
                        id:included
                        checked: false
                        onClicked:{
                            console.log("test", this.checked)
                            model.selected=this.checked
                            checkedBu.forEach(function(button){
                                //skip if it is the current handler or the button is not checked
                                if(!internal.checked || button == this || this.checked)return
                                button.checked = false;
                                checkedBu.pop(button)

                            })
                            console.log("checked list", !(this in checkedBu), this.checked)
                            if(!(this in checkedBu) && this.checked)checkedBu.push(this);
                            if((this in checkedBu) && !this.checked)checkedBu.pop(this);
                            console.log("Test", this.checked, "in list", this in checkedBu,"model", model.selected)
                        }

                }
                Label {
                    wrapMode:Label.Wrap
                    width:parent.width/4-units.gu(1)
                    text: model.name
                    font.bold: true
                    //padding:units.gu(1)
                }

                TextField {
                    id:relativeAmt
                    inputMethodHints:Qt.ImhFormattedNumbersOnly
                    visible:included.checked&&!internal.checked&&!equal.checked
                    width:parent.width/4-units.gu(1)
                    placeholderText: "%"
                    hasClearButton:false
                    onTextChanged:{
                        model.relative=this.text
                        if(relativeLock && !(absoluteLock))return
                        absoluteLock = true
                        //calculate absolute number and write them into the other txtfield
                        if(relativeAmt.text != ""){
                            absoluteAmt.text=Math.round(parseFloat(relativeAmt.text)*parseFloat(summed.text))/100
                        }else{
                            absoluteAmt.text = ""
                        }
                        check(viewer.model)
                        absoluteLock = false
                    }
                    // Bind the text property to the ListElement's property
                }
                 TextField {
                     id:absoluteAmt
                    inputMethodHints:Qt.ImhFormattedNumbersOnly
                    hasClearButton:false
                    visible:included.checked&&!internal.checked&&!equal.checked
                    width:parent.width/4-units.gu(1)
                    placeholderText: groupinfo.defaultCurrencyCode
                    // Bind the text property to the ListElement's property
                    onTextChanged:{
                        model.absolute=this.text
                        if(!(relativeLock) && absoluteLock)return
                        relativeLock=true
                        //calculate absolute number and write them into the other txtfield
                        if(absoluteAmt.text != ""){
                            relativeAmt.text=Math.round(parseFloat(absoluteAmt.text)/parseFloat(summed.text)*10000)/100
                        }else{
                            relativeAmt.text = ""
                        }
                        checkModel()
                        relativeLock=false
                    }
                }

            }
        }
    }
}

