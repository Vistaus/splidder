import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Lomiri.Components.Popups 1.3
import "libs/settings.js" as Setting
import Qt.labs.settings 1.0

Page {
    property string preGroupName :""
    property string groupId :""
    property string currency : ""
    property var available : {}
    id:groupSetPage
    width: bottomEdge.width
    height: bottomEdge.height
    header: PageHeader {
        id:gSetHead
        title: i18n.tr("Group Settings")
    }
property int originalHeight : 0
    FailDia{
        id:fail
    }
    Component{
        id:curCh
        CurrencyChoose{

        }
    }



    property string latestMessage : ""
    Flickable{
        anchors {
        top: gSetHead.bottom
        left: parent.left
        right: parent.right
        bottom: parent.bottom
        }
        width:parent.width
        Column{
            id:cols
            anchors.fill:parent
            spacing:1
            Row{
                Label{
                    text:i18n.tr("Group name")
                    width:cols.width/2
                }
                TextField{
                    id:groupNameField
                    placeholderText:i18n.tr("Group name")
                    text:preGroupName
                    width:cols.width/2
                }
            }
            Row{
                Label{
                    width:cols.width/2
                    text:i18n.tr("Currency")
                    id:cuLab
                }
                Button{
                    width:cols.width/2
                    text:currency=="DOGE"?i18n.tr("Please choose"):currency
                    onClicked:{
                        Setting.getRates(function(status, rates){
                            if(status){
                                available = rates["result"]
                                PopupUtils.open(curCh)
                            }

                        });
                    }
                }

            }
            Button{
                enabled:groupNameField.text!=""&&currency!="DOGE"
                color:LomiriColors.green
                iconName:"ok"
                text:i18n.tr("Safe Settings")
                width:parent.width
                onClicked:{
                    //
                    Setting.safeSet(available,groupId, "1969-12-31T00:00:00.000Z", currency, groupNameField.text, function(status, response){
                        if(status)console.log("Success")
                        var i = 0;
                        sets.loginData.forEach(function(key){
                            if(key["objectId"]==groupId){
                                console.log("FOUND", i, groupId, groupNameField.text)
                                var newSet = sets.loginData;
                                newSet[i].alias = groupNameField.text;
                                newSet[i].defaultCurrencyCode = currency;
                                sets.loginData = newSet;
                                root.groupListing.remove(i);
                                return;
                            }
                            i++;

                        })
                        root.groupListing.append({id:groupId, name:groupNameField.text, index:i-1});

                        if(status)mainPage.pageStack.removePages(groupSetPage)
                           /// try{
                                //mainPage.pageStack.removePages(joinPage)
                                root.currentGroup=groupId
                                mainPage.pageStack.addPageToNextColumn(mainPage, Qt.resolvedUrl("group.qml"));
                           // }/*
                            // catch(e){
                            //     console.log("joinPage not existing")
                            // }*/
                    })
                }
            }
        }

    }
}

