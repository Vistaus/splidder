/*
 * Copyright (C) 2024  S60W79
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * splider is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "libs/login.js" as Login
import Qt.labs.settings 1.0
import Lomiri.Components.Popups 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'splider.s60w79'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)


    property string currentGroup : ""
    property var groupInfos : []
    property alias groupListing : paymentsModel
    property double highSaldo:-100000000000000
    property double lowSaldo:100000000000000
    Settings {
        id:sets
        property var groupCodes : []
        property var loginData : []
        property int welcomeversion:0
        //cash of group payment data.

    }

    Connections {
    target: UriHandler

    onOpened: {

        if (uris.length > 0) {
            console.log("URL detected")
            console.log(uris)
            const url = uris[0].replace("https://splid.net/j/", "").replace("http://splid.net/j/", "")
            console.log("URL:", url)
            joinHelper(url);
        }
    }
}
Component.onCompleted: {
   // sets.groupCodes = []
   // sets.loginData = []
    if (Qt.application.arguments && Qt.application.arguments.length > 0) {

        for (var i = 0; i < Qt.application.arguments.length; i++) {
            if (Qt.application.arguments[i].includes("splid.net")) {
                console.log("Incoming Call on Closed App")
                const code = Qt.application.arguments[i].replace("https://splid.net/j/", "").replace("http://splid.net/j/", "")
                joinHelper(code)
            }
        }
    }
    //open welcome dialog
    if(sets.welcomeversion<1)PopupUtils.open(welcomeDia)
    sets.welcomeversion = 1
}
AdaptivePageLayout {
    anchors.fill:parent
    primaryPage:mainPage
    Page {
          Component {
        id:welcomeDia
        Welcome{

        }
    }
        anchors.fill: parent
        id:mainPage
        header: PageHeader {
            id: header
            title: i18n.tr('Splider')
             trailingActionBar.actions: [
                Action {
                    iconName: "add"
                    text: i18n.tr('Join or create group')
                    onTriggered:mainPage.pageStack.addPageToCurrentColumn(mainPage, Qt.resolvedUrl("create.qml"));
                }
            ]
            leadingActionBar.actions : [
                Action {
                    text : i18n.tr("About the app")
                    iconSource : Qt.resolvedUrl("../assets/split.png")
                    onTriggered : {
                        mainPage.pageStack.addPageToNextColumn(mainPage, Qt.resolvedUrl("Information.qml"))
                    }
                }
            ]

        }

        Label {
            id:startMessage
            visible:sets.loginData.length == 0
            anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            text: i18n.tr('You are not logged in any groups yet.\nSwipe upwards to join or create a group')

            verticalAlignment: Label.AlignVCenter
            horizontalAlignment: Label.AlignHCenter
        }
        Component.onCompleted: {

            if(sets.loginData.length!=0){
                var i = 0;
                sets.loginData.forEach(function(key){
                    paymentsModel.append({id:key["objectId"], name:key["alias"], index:i});
                    i++;
                })
            }
        }
        ListView {
            id:groupLst
                anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            width: parent.width
            height: parent.height
            //visible:sets.loginData.length !=0
            model: ListModel {
                id:paymentsModel
            }

             delegate: Component{
                 Column{
            width:parent.width
            spacing:1
                 ListItem {
                        id:groupIt
                     leadingActions: ListItemActions {
                            actions: [
                                Action {
                                    iconName: "delete"
                                    onTriggered:{
                                        var tmpLoginData = sets.loginData
                                        var tmpCodes = sets.groupCodes
                                        sets.loginData.forEach(function(key){
                                            //if not a match, skip, else delete all group data
                                            if(key["objectId"] != model.id)return
                                            //keyset
                                            tmpLoginData.pop(key);
                                            //invite code
                                            tmpCodes.pop(key["shortCode"])
                                            paymentsModel.remove(model.index)
                                        })
                                        //save changes
                                        sets.loginData = tmpLoginData;
                                        sets.groupCodes = tmpCodes;
                                        groupIt.destroy()
                                    }
                                }
                            ]
                        }
                    ListItemLayout {
                        title.text: model.name
                    }
                    onClicked:{
                        root.currentGroup = model.id

                        Login.groupinfo(model.id, "1969-12-31T00:00:00.000Z", 0, 100, function(status, groupData){
                            if(status){
                                 root.groupInfos.push(groupData["result"]["results"][0])
                                 mainPage.pageStack.addPageToNextColumn(mainPage, Qt.resolvedUrl("group.qml"));
                            }
                        });
                    }
                    }

            }
        }
      }
        BottomEdge {
            id: bottomEdge
            hint.text: i18n.tr("Swipe to join or create group")
            height:Qt.inputMethod.keyboardRectangle.height>0?Math.max(header.height*25,mainPage.height-header.height):Math.max(header.height*10, mainPage.height-header.height)
            hint.status:BottomEdgeHint.Active
            // override bottom edge sections to switch to real content
            contentUrl:Qt.resolvedUrl("create.qml");
            anchors.fill:mainPage
            BottomEdgeRegion {
                to:0.4
                from:0
                contentUrl:Qt.resolvedUrl("join.qml")
            }
        }
    }
}
    function joinHelper(code){


    Login.log(code, function(status, logData){
        if(status){

        Login.groupinfo(logData["result"]["objectId"], "1969-12-31T00:00:00.000Z", 0, 100, function(status, groupData){
            if(status){
                root.groupInfos.push(groupData["result"]["results"][0])
                root.currentGroup = logData["result"]["objectId"]

                //if group is already in memory, skip the next fiew steps:
                if(sets.groupCodes.includes(code))return;

                var tmp = sets.groupCodes;
                tmp.push(code)
                sets.groupCodes = tmp
                var keys = logData["result"]
                keys.alias = groupData["result"]["results"][0]["name"]
                var tmp = sets.loginData
                tmp.push(keys);
                sets.loginData = tmp;
                root.groupListing.append({id:root.currentGroup, name:keys.alias, invite:code})
                mainPage.pageStack.addPageToNextColumn(mainPage, Qt.resolvedUrl("group.qml"));
            }

        })

        }
    });

}
}
