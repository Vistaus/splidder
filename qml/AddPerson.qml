import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Lomiri.Components.Popups 1.3
import "libs/add.js" as Add

    //just a text field dialog with two buttons.
         Dialog {

             id: regDia
             title: i18n.tr("Add Person")
             text: i18n.tr("Add a group member. For multiple Members, separate Names by ;")
              TextField{
                id:memberName
                placeholderText:i18n.tr("Person's name")
                width:parent.width
                onAccepted:{
                    //add person and close window

                    Add.person(memberName.text, root.currentGroup)
                }
            }
             Button {
                 text: i18n.tr("Add")
                 color: LomiriColors.green
                 onClicked:{
                     //add person and close window
                    Add.person(memberName.text, root.currentGroup)
                 }
             }
             Button {
                 text: i18n.tr("Abort")
                 color: LomiriColors.red
                 onClicked: PopupUtils.close(regDia)
             }
         }

