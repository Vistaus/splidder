import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Lomiri.Components.Popups 1.3
import "libs/add.js" as Add

    //just a text field dialog with two buttons.
         Dialog {

             id: regDia
             title: i18n.tr("Welcome")
             text: i18n.tr("Thanks for choosing Splider!")
             Icon {
          anchors.horizontalCenter: parent.horizontalCenter
          height: Math.min(parent.width/2, parent.height/2)
          width:height
          source:Qt.resolvedUrl("../assets/split.png")
          layer.enabled: true
          layer.effect: LomiriShapeOverlay {
              relativeRadius: 0.75
           }
        }
           Label {
               horizontalAlignment:Text.AlignHCenter
               width:parent.width
                    wrapMode:Label.Wrap
                    text: i18n.tr("Cross Platform")
                    font.bold: true
                    //padding:units.gu(1)
                }
            Label {

               width:parent.width
                    wrapMode:Label.Wrap
                    text: i18n.tr("With this app you can share and split the bills cross platform. The Splid app uses the same API and works an IOS and Android.")

                    //padding:units.gu(1)
                }
            Label {
               horizontalAlignment:Text.AlignHCenter
               width:parent.width
                    wrapMode:Label.Wrap
                    text: i18n.tr("Easy to use")
                    font.bold: true
                    //padding:units.gu(1)
                }
            Label {

               width:parent.width
                    wrapMode:Label.Wrap
                    text: i18n.tr("See on one view the saldo (in green) and Absolute Payments of each group Member.\nFurther there are Icons for the payment categories and some other features, missing in the original, closed-source app.")

                    //padding:units.gu(1)
                }
            Label {
               horizontalAlignment:Text.AlignHCenter
               width:parent.width
                    wrapMode:Label.Wrap
                    text: i18n.tr("Yet so much to do!")
                    font.bold: true
                    //padding:units.gu(1)
                }
                Label {

               width:parent.width
                    wrapMode:Label.Wrap
                    text: i18n.tr("Thought there are many features to be implemented: Search filters, CSV Export, further overview, translations, ...\nAlso reverse engineering this awfull API alone was cruel.\nSo I'd be really, really happy about support, wether as a commitment to this project or a small donation (:")

                    //padding:units.gu(1)
                }
            Button {
                iconName:"preferences-desktop-login-items-symbolic"
                 text: i18n.tr("Commit to the project")
                 onClicked: Qt.openUrlExternally("https://gitlab.com/S60W79/splidder/-/issues/1")
             }
             Button {
                 iconName:"like"
                 text: i18n.tr("Donate")
                 onClicked: Qt.openUrlExternally("https://infoportal.ddns.net/entwickler/spenden/s60w79.php")
             }
             Button {
                 text: i18n.tr("Close")
                 onClicked: PopupUtils.close(regDia)
             }
         }

