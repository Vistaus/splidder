import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Lomiri.Components.Popups 1.3
import "libs/add.js" as Add


Dialog {

    id: regDia
    title: internal.checked?i18n.tr("Payment"):i18n.tr("Spending")
    text: latestMessage
    property string category:"unspecified"

    TextField{
        id:summed
        inputMethodHints:Qt.ImhFormattedNumbersOnly
        height:units.gu(5)
        width:parent.width
        placeholderText:i18n.tr("Amount")
    }
    Row{
        z:3
        Label{
            text:i18n.tr("Internal payment:")
        }
        Switch{

            id:internal
        }
    }
    TextField{
        visible:!internal.checked
        id:name
        height:units.gu(5)
        width:parent.width
        placeholderText:i18n.tr("Title")
    }
    Component{
        id:catSwitchDia
    CategoryChoose{


    }
    }

    Row{
        visible:!internal.checked
        z:3
        Label{
            text:i18n.tr("Distribute equavally")
        }
        Switch{
            checked:true
            visible:!internal.checked
            id:equal
        }
    }
    Label{
        horizontalAlignment:Text.AlignHCenter
        font.bold:true
        z:3
        text:i18n.tr("Payer(s)")
    }

    Flickable{
        z:1
        width:parent.width
        height:units.gu(15)
        KeyList{
            id:payerLst
            property int checkedNr : -1
            z:1
            model: ListModel {
                id:payerChModel

            }
            function checkModel(){ check(payerChModel) }
        }
    }
     Label{
        horizontalAlignment:Text.AlignHCenter
        font.bold:true
        z:3
        text:internal.checked?i18n.tr("Recipient"):i18n.tr("Consumer(s)")
    }
    Flickable{
        z:1
        width:parent.width
        height:units.gu(15)
        KeyList{
            id:recipientLst
            z:1
            model: ListModel {
                id:recChModel
            }
            function checkModel(){ check(recChModel) }
        }
    }
    Row{
        Label{
            visible:false
        id:warning
        color: LomiriColors.red
        text:i18n.tr("Mismatch in the distribution")
        }
        Label{
            visible:false
            id:mismatchNr
            text:""
        }
    }
    Row{
        z:3
        width:parent.width
        visible:!internal.checked
        Button{
            id:catBu
            width:parent.width*0.9
            text: i18n.tr("Category")
            onClicked:PopupUtils.open(catSwitchDia)
        }
        Icon {
            source:category in cat2icon?Qt.resolvedUrl("../assets/"+cat2icon[category]+".png"):Qt.resolvedUrl("../assets/unknown.png")
            height:catBu.height
            width:parent.width*0.1

        }
    }
    Button{
        id:adder
        enabled:internal.checked || equal.checked
        z:3
        text: i18n.tr("Add")
        color: LomiriColors.green
        onClicked:{
            console.log("adding")
            //payer handling
            let primaryPayer = ""
            let secondaryPayers={}
            let balance = {}
            let numPayer = 0
            let numConsumer = 0
            //determination of number of the payers and recipients
            for (var i = 0; i < payerChModel.count; i++)if(payerChModel.get(i).selected)numPayer++;
            for (var i = 0; i < recChModel.count; i++)if(recChModel.get(i).selected)numConsumer++;
            //go threw all payers
            for (var i = 0; i < payerChModel.count; i++) {
                if(!(payerChModel.get(i).selected))continue
                if(primaryPayer == ""){
                    primaryPayer=payerChModel.get(i).id
                }else{
                    // preset: split equal
                    secondaryPayers[payerChModel.get(i).id]=parseFloat(summed.text)/numPayer
                    //only if not split equavally
                    if(!(equal.checked) && !(internal.checked))secondaryPayers[payerChModel.get(i).id]=parseFloat(payerChModel.get(i).absolute)
                }

            }
            for (var i = 0; i < recChModel.count; i++) {
                if(!(recChModel.get(i).selected))continue
                // preset: split equal
                balance[recChModel.get(i).id]=1/numConsumer
                //only if not split equavally
                if(!(equal.checked) && !(internal.checked))balance[recChModel.get(i).id]=parseFloat(recChModel.get(i).relative)/100


            }
            // go threw all recipients
            console.log("PRIM", primaryPayer, "SEC", JSON.stringify(secondaryPayers), "CON", JSON.stringify(balance))
            Add.payment(root.currentGroup, internal.checked, parseFloat(summed.text), balance, name.text, groupinfo.defaultCurrencyCode, primaryPayer, secondaryPayers, category, function(success, txt){
                if(success)PopupUtils.close(regDia)
            })
        }
    }

    Button {
        z:3

        text: i18n.tr("Close")
        color: LomiriColors.red
        onClicked: PopupUtils.close(regDia)
    }

    Component.onCompleted:{
        var i = 0;
        groupPage.members.forEach(function(member){
            payerChModel.append({name:member.name, id:member.GlobalId, index:i, absolute:"", relative:"", selected:false})
            recChModel.append({name:member.name, id:member.GlobalId, index:i, absolute:"", relative:"", selected:false})
            i++;
        })
    }
    function check(model){
        var sum = 0
        console.log("checking")
        for (var i = 0; i < model.count; i++) {
            console.log("at", i)
            if(!(parseFloat(model.get(i).relative))){
                //skip if e.g. letters in a textfield
                warning.visible=true
                continue;
            }
            console.log(model.get(i).rrelative)
            sum+=parseFloat(model.get(i).relative)
        }
        if(sum < 99 || sum > 101){
            warning.visible=true
            mismatchNr.text = sum+"%"
            mismatchNr.visible=true
            adder.enabled=false
            return
        }
        adder.enabled=true
        warning.visible = false
        mismatchNr.visible=false
    }
}

