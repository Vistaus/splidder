import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Lomiri.Components.Popups 1.3
import "libs/login.js" as Login
import Qt.labs.settings 1.0

Page {
    id:joinPage
    width: bottomEdge.width
    height: bottomEdge.height
    header: PageHeader {
        id:scanHead
        title: i18n.tr("Join")
    }
property int originalHeight : 0
    FailDia{
        id:fail
    }


    property string latestMessage : ""
    Flickable{
        anchors {
        top: scanHead.bottom
        left: parent.left
        right: parent.right
        bottom: parent.bottom
        }
        width:parent.width
        Column{
            id:cols
            anchors.fill:parent
            spacing:1

        Row{
            width:parent.width
        TextField{
            id:code
            placeholderText:i18n.tr("Invite code")
            width:parent.width*0.9
            onAccepted:joinGroup()
        }
        
        Button{
            iconName:"ok"
            color:LomiriColors.green
            width:parent.width*0.1
            text:i18n.tr("OK")
            onClicked:joinGroup()
            
            }
        }
        Button{
            id:groupBu
            text:"Create new group"
            width:parent.width
            onClicked:mainPage.pageStack.addPageToNextColumn(mainPage, Qt.resolvedUrl("create.qml"));
        }
        }
    }
    function joinGroup(){
    console.log("height", Qt.inputMethod.keyboardRectangle.height)


    Login.log(code.text, function(status, logData){
        if(status){

        Login.groupinfo(logData["result"]["objectId"], "1969-12-31T00:00:00.000Z", 0, 100, function(status, groupData){
            if(status){
                root.groupInfos.push(groupData["result"]["results"][0])
                root.currentGroup = logData["result"]["objectId"]
                mainPage.pageStack.addPageToNextColumn(mainPage, Qt.resolvedUrl("group.qml"));
                // if group is already in memory, skip the next fiew steps.
                if(sets.groupCodes.includes(code.text))return;
                var tmp = sets.groupCodes;
                tmp.push(code.text)
                sets.groupCodes = tmp
                var keys = logData["result"]
                keys.alias = groupData["result"]["results"][0]["name"]
                var tmp = sets.loginData
                tmp.push(keys);
                sets.loginData = tmp;
                root.groupListing.append({id:root.currentGroup, name:keys.alias})
            }else{
                latestMessage = groupData;
                PopupUtils.open(fail)
            }

        })

        }else{
            latestMessage = logData;
            PopupUtils.open(fail);
        }
    });

}
}

