/*
*/

import QtQuick 2.9
import Lomiri.Components 1.3

//TODO: Avoid loading this on every element
//try send signals for delete and refresh
import QtQuick.LocalStorage 2.0

ListItem {
    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter
    clip: true
    height:layout.height
    //highlightColor: Lomiri.color


    ListItemLayout {
        id:layout
        title.text: model.name
        title.font.bold: true

        subtitle.text: model.spendings?model.percent+"%":model.saldo+model.curency
        subtitle.color:model.spendings?LomiriColors.warmGrey:model.saldo<0?LomiriColors.red:LomiriColors.green
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            SlotsLayout.position: SlotsLayout.Trailing
            text: model.spendings?model.absolute:model.absolute+model.curency
            color:LomiriColors.warmGrey
            //color: theme.palette.normal.baseText
        }

        //ProgressionSlot {}

        //Let's connect with the main ticker
    }

    ProgressBar {
        id: bar
        width: parent.width
        anchors.bottom: parent.bottom
        maximumValue: 100
        minimumValue: 0
        value: model.spendings?model.percent:root.highSaldo!=root.lowSaldo?((model.saldo-root.lowSaldo)/(root.highSaldo-root.lowSaldo))*100:50//!=0?model.saldo:1
    }
}

