function send(id, transaction, amount, consumers, title, currency, mainPayer, secondaries, callback) {
    // Create a new XMLHttpRequest object
    const xhr = new XMLHttpRequest();

    // Set up the request
    xhr.open('POST', 'https://splid.herokuapp.com/parse/classes/Entry', true);

    // Set request headers
    xhr.setRequestHeader('Host', 'splid.herokuapp.com');
    xhr.setRequestHeader('X-Parse-Application-Id', 'AKCaB0FCF0NIigWjxcDBpDYh7q6eN7gYfKxk5QBN');
    xhr.setRequestHeader('X-Parse-App-Build-Version', '142002');
    xhr.setRequestHeader('X-Parse-App-Display-Version', '1.4.2');
    xhr.setRequestHeader('X-Parse-Os-Version', '7.1.2');
    xhr.setRequestHeader('User-Agent', 'Parse Android SDK API Level 25');
    xhr.setRequestHeader('X-Parse-Installation-Id', 'fecd381e-921e-4642-ab65-dd1a789d5cb8');
    xhr.setRequestHeader('X-Parse-Client-Key', '4Z29DJvRGdVnB5dcTvDTTG01fbkITxvcPCPOt21M');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Accept-Encoding', 'gzip, deflate, br');
    xhr.setRequestHeader('Connection', 'close');

    xhr.onload = function () {
        if (xhr.status >= 200 && xhr.status < 300) {
            callback(true, JSON.parse(xhr.responseText))
        } else {
            callback(false, xhr.statusText);
        }
    };

    xhr.onerror = function () {
        callback(false, xhr.statusText);
    };

    xhr.send(JSON.stringify({"group":{"__type":"Pointer","className":"_User","objectId":id},"createdGlobally":{"__type":"Date","iso":"2024-01-20T17:30:17.101Z"},"date":{"__op":"Delete"},"UpdateID":"26557003-b618-4ec4-a664-3e8eccb11134","UpdateInstallationID":"fecd381e-921e-4642-ab65-dd1a789d5cb8","isPayment":false,"items":[{"AM":amount,"P":{"PT":0,"P":consumers}}],"title":title,"category":{"__op":"Delete"},"GlobalId":"a57358f3-c8dc-4c07-8da3-d57040b539d0","currencyCode":currency,"primaryPayer":mainPayer,"secondaryPayers":secondaries}));
}
