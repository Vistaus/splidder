function payments(id, date, skip, limit, callback){
        // Create a new XMLHttpRequest object
    const xhr = new XMLHttpRequest();

    // Set up the request
    xhr.open('POST', 'https://splid.herokuapp.com/parse/functions/findObjects', true);

    // Set request headers
    xhr.setRequestHeader('Host', 'splid.herokuapp.com');
    xhr.setRequestHeader('X-Parse-Application-Id', 'AKCaB0FCF0NIigWjxcDBpDYh7q6eN7gYfKxk5QBN');
    xhr.setRequestHeader('X-Parse-App-Build-Version', '142002');
    xhr.setRequestHeader('X-Parse-App-Display-Version', '1.4.2');
    xhr.setRequestHeader('X-Parse-Os-Version', '7.1.2');
    xhr.setRequestHeader('User-Agent', 'Parse Android SDK API Level 25');
    xhr.setRequestHeader('X-Parse-Installation-Id', 'fecd381e-921e-4642-ab65-dd1a789d5cb8');
    xhr.setRequestHeader('X-Parse-Client-Key', '4Z29DJvRGdVnB5dcTvDTTG01fbkITxvcPCPOt21M');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Accept-Encoding', 'gzip, deflate, br');
    xhr.setRequestHeader('Connection', 'close');

    xhr.onload = function () {
        if (xhr.status >= 200 && xhr.status < 300) {
            console.log(xhr.responseText)
            callback(true, JSON.parse(xhr.responseText))
        } else {
            callback(false, xhr.statusText);
        }
    };

    xhr.onerror = function () {
        callback(false, xhr.statusText);
    };

    xhr.send(JSON.stringify({ "className":"Entry","group":id,"minDate":{"__type":"Date","iso":date},"skip":skip,"limit":limit }));

}


function members(id, date, skip, limit, callback){
        // Create a new XMLHttpRequest object
    const xhr = new XMLHttpRequest();

    // Set up the request
    xhr.open('POST', 'https://splid.herokuapp.com/parse/functions/findObjects', true);

    // Set request headers
    xhr.setRequestHeader('Host', 'splid.herokuapp.com');
    xhr.setRequestHeader('X-Parse-Application-Id', 'AKCaB0FCF0NIigWjxcDBpDYh7q6eN7gYfKxk5QBN');
    xhr.setRequestHeader('X-Parse-App-Build-Version', '142002');
    xhr.setRequestHeader('X-Parse-App-Display-Version', '1.4.2');
    xhr.setRequestHeader('X-Parse-Os-Version', '7.1.2');
    xhr.setRequestHeader('User-Agent', 'Parse Android SDK API Level 25');
    xhr.setRequestHeader('X-Parse-Installation-Id', 'fecd381e-921e-4642-ab65-dd1a789d5cb8');
    xhr.setRequestHeader('X-Parse-Client-Key', '4Z29DJvRGdVnB5dcTvDTTG01fbkITxvcPCPOt21M');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Accept-Encoding', 'gzip, deflate, br');
    xhr.setRequestHeader('Connection', 'close');

    xhr.onload = function () {
        if (xhr.status >= 200 && xhr.status < 300) {
            callback(true, JSON.parse(xhr.responseText))
        } else {
            callback(false, xhr.statusText);
        }
    };

    xhr.onerror = function () {
        callback(false, xhr.statusText);
    };

    xhr.send(JSON.stringify({ "className":"Person","group":id,"minDate":{"__type":"Date","iso":date},"skip":skip,"limit":limit }));

}
function resolveName(term){
    //turn back the name of a member.
          var toReturn = "?"
    groupPage.members.forEach(function(member){
        console.log("SEARCHING:", member["GlobalId"], term)
        if(member["UpdateInstallationID"]==term || member["UpdateID"] == term || member["GlobalId"] == term || member["objectId"] == term)toReturn =(member["name"])
    })
    return toReturn
}

function sumPayments(pid){
    var sum = 0;
    //sum up all paymants from a person's ID.
    groupPage.payments.forEach(function(spending){
        var additional = 0;
        if(spending["isDeleted"])return
        if(spending.primaryPayer != pid){
            if ("secondaryPayers" in spending){
                //add secondary payment if involved in such an.
                console.log("SECONDARY", additional);
                if (pid in spending.secondaryPayers)additional+=spending["secondaryPayers"][pid]
                console.log("SECONDARY", additional);
            }
            //all already done for this round.
            sum+=additional
            return
        }
        //then in normal cases, the summand is equal to the spending
        additional = spending["items"][0]["AM"]
        if ("secondaryPayers" in spending){
            //accept more payers are involved, then subtract the other spendings from the whole price.
            Object.keys(spending.secondaryPayers).forEach(function(secPayer){
                console.log("SECONDARY", additional);
                additional -= spending["secondaryPayers"][secPayer];
                console.log("SECONDARY", additional);
            })
        }
        //sum up
        console.log(additional)
        sum += additional
    })
    console.log(sum)
    return sum
}
function consumtions(pid){

    //calculate person's consumptions by pid.
    var sum = 0;
     groupPage.payments.forEach(function(spending){
         if(spending["isDeleted"])return
        const spendingAmt = spending["items"][0]["AM"]
        if(!(pid in spending["items"][0]["P"]["P"]))return;
        //if involved, sum up
        sum+=spendingAmt*spending["items"][0]["P"]["P"][pid]
    });
    return sum;
}


