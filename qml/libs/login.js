function log(code, callback) {
    // Create a new XMLHttpRequest object
    const xhr = new XMLHttpRequest();

    // Set up the request
    xhr.open('POST', 'https://splid.herokuapp.com/parse/functions/joinGroupWithAnyCode', true);

    // Set request headers
    xhr.setRequestHeader('Host', 'splid.herokuapp.com');
    xhr.setRequestHeader('X-Parse-Application-Id', 'AKCaB0FCF0NIigWjxcDBpDYh7q6eN7gYfKxk5QBN');
    xhr.setRequestHeader('X-Parse-App-Build-Version', '142002');
    xhr.setRequestHeader('X-Parse-App-Display-Version', '1.4.2');
    xhr.setRequestHeader('X-Parse-Os-Version', '7.1.2');
    xhr.setRequestHeader('User-Agent', 'Parse Android SDK API Level 25');
    xhr.setRequestHeader('X-Parse-Installation-Id', 'fecd381e-921e-4642-ab65-dd1a789d5cb8');
    xhr.setRequestHeader('X-Parse-Client-Key', '4Z29DJvRGdVnB5dcTvDTTG01fbkITxvcPCPOt21M');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Accept-Encoding', 'gzip, deflate, br');
    xhr.setRequestHeader('Connection', 'close');

    xhr.onload = function () {
        if (xhr.status >= 200 && xhr.status < 300) {
            callback(true, JSON.parse(xhr.responseText))
        } else {
            callback(false, xhr.statusText);
        }
    };

    xhr.onerror = function () {
        callback(false, xhr.statusText);
    };

    xhr.send(JSON.stringify({ code }));
}

function create(code, callback) {
    // Create a new XMLHttpRequest object
    const xhr = new XMLHttpRequest();

    // Set up the request
    xhr.open('POST', 'https://splid.herokuapp.com/parse/functions/createGroup', true);

    // Set request headers
    xhr.setRequestHeader('Host', 'splid.herokuapp.com');
    xhr.setRequestHeader('X-Parse-Application-Id', 'AKCaB0FCF0NIigWjxcDBpDYh7q6eN7gYfKxk5QBN');
    xhr.setRequestHeader('X-Parse-App-Build-Version', '142002');
    xhr.setRequestHeader('X-Parse-App-Display-Version', '1.4.2');
    xhr.setRequestHeader('X-Parse-Os-Version', '7.1.2');
    xhr.setRequestHeader('User-Agent', 'Parse Android SDK API Level 25');
    xhr.setRequestHeader('X-Parse-Installation-Id', 'fecd381e-921e-4642-ab65-dd1a789d5cb8');
    xhr.setRequestHeader('X-Parse-Client-Key', '4Z29DJvRGdVnB5dcTvDTTG01fbkITxvcPCPOt21M');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Accept-Encoding', 'gzip, deflate, br');
    xhr.setRequestHeader('Connection', 'close');

    xhr.onload = function () {
        if (xhr.status >= 200 && xhr.status < 300) {
            console.log(xhr.responseText)
            callback(true, JSON.parse(xhr.responseText))
        } else {
            callback(false, xhr.statusText);
        }
    };

    xhr.onerror = function () {
        callback(false, xhr.statusText);
    };

    xhr.send(JSON.stringify({ code }));
}


function groupinfo(id, date, skip, limit, callback){
        // Create a new XMLHttpRequest object
    const xhr = new XMLHttpRequest();

    // Set up the request
    xhr.open('POST', 'https://splid.herokuapp.com/parse/functions/findObjects', true);

    // Set request headers
    xhr.setRequestHeader('Host', 'splid.herokuapp.com');
    xhr.setRequestHeader('X-Parse-Application-Id', 'AKCaB0FCF0NIigWjxcDBpDYh7q6eN7gYfKxk5QBN');
    xhr.setRequestHeader('X-Parse-App-Build-Version', '142002');
    xhr.setRequestHeader('X-Parse-App-Display-Version', '1.4.2');
    xhr.setRequestHeader('X-Parse-Os-Version', '7.1.2');
    xhr.setRequestHeader('User-Agent', 'Parse Android SDK API Level 25');
    xhr.setRequestHeader('X-Parse-Installation-Id', 'fecd381e-921e-4642-ab65-dd1a789d5cb8');
    xhr.setRequestHeader('X-Parse-Client-Key', '4Z29DJvRGdVnB5dcTvDTTG01fbkITxvcPCPOt21M');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Accept-Encoding', 'gzip, deflate, br');
    xhr.setRequestHeader('Connection', 'close');

    xhr.onload = function () {
        if (xhr.status >= 200 && xhr.status < 300) {
            const returnal = JSON.parse(xhr.responseText)
            if(returnal.result.results == []){
                callback(false, i18n.tr("Please adjust group settings"))
            }else{
            callback(true, returnal)
            }

        } else {
            console.log("ERROR", xhr.responseText)
            callback(false, xhr.statusText);
        }
    };

    xhr.onerror = function () {
        console.log("ERROR", xhr.responseText)
        callback(false, xhr.statusText);
    };

    xhr.send(JSON.stringify({ "className":"GroupInfo","group":id,"minDate":{"__type":"Date","iso":date},"skip":skip,"limit":limit }));

}

function joinGroup(){
    log(code.text, function(status, logData){
        if(status){

        groupinfo(logData["result"]["objectId"], "1969-12-31T00:00:00.000Z", 0, 100, function(status, groupData){
            if(status){
                root.groupInfos.push(groupData["result"]["results"][0])
                root.currentGroup = logData["result"]["objectId"]
                mainPage.pageStack.addPageToNextColumn(mainPage, Qt.resolvedUrl("../group.qml"));
                // if group is already in memory, skip the next fiew steps.
                if(sets.groupCodes.includes(code.text))return;
                var tmp = sets.groupCodes;
                tmp.push(code.text)
                sets.groupCodes = tmp
                var keys = logData["result"]
                keys.alias = groupData["result"]["results"][0]["name"]
                var tmp = sets.loginData
                tmp.push(keys);
                sets.loginData = tmp;
                root.groupListing.append({id:root.currentGroup, name:keys.alias})
            }else{
                latestMessage = groupData;
                PopupUtils.open(fail)
            }

        })

        }else{
            latestMessage = logData;
            PopupUtils.open(fail);
        }
    });

}


function createGroup(){
    create(code.text, function(status, logData){
        if(status){

        //simulate Group Info call (would return no results for group stub)
        const groupData = {"result":{"serverDate":{"__type":"Date","iso":"2024-01-25T13:53:19.741Z"},"results":[{name:i18n.tr("New Group"), "group":{"__type":"Pointer","className":"_User","objectId":logData["result"]["objectId"]},defaultCurrencyCode:"DOGE", "createdGlobally":{"__type":"Date","iso":"2024-01-25T13:53:19.741Z"}}]}}
        root.groupInfos.push(groupData["result"]["results"][0])
        root.currentGroup = logData["result"]["objectId"]
        mainPage.pageStack.addPageToNextColumn(mainPage, Qt.resolvedUrl("../groupSettings.qml"), {preGroupName:i18n.tr("New group"), groupId:root.currentGroup, currency:"DOGE"});
        // if group is already in memory, skip the next fiew steps.
        if(sets.groupCodes.includes(code.text))return;
        var tmp = sets.groupCodes;
        tmp.push(code.text)
        sets.groupCodes = tmp
        var keys = logData["result"]
        keys.alias = groupData["result"]["results"][0]["name"]
        var tmp = sets.loginData
        tmp.push(keys);
        sets.loginData = tmp;
        root.groupListing.append({id:root.currentGroup, name:keys.alias})
        mainPage.pageStack.removePages(mainPage.pageStack.primaryPage)



        }else{
            latestMessage = logData;
            PopupUtils.open(fail);
        }
    });

}
