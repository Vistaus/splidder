function getRates(callback){
        // Create a new XMLHttpRequest object
    const xhr = new XMLHttpRequest();

    // Set up the request
    xhr.open('POST', 'https://splid.herokuapp.com/parse/functions/getCurrencyRates', true);

    // Set request headers
    xhr.setRequestHeader('Host', 'splid.herokuapp.com');
    xhr.setRequestHeader('X-Parse-Application-Id', 'AKCaB0FCF0NIigWjxcDBpDYh7q6eN7gYfKxk5QBN');
    xhr.setRequestHeader('X-Parse-App-Build-Version', '142002');
    xhr.setRequestHeader('X-Parse-App-Display-Version', '1.4.2');
    xhr.setRequestHeader('X-Parse-Os-Version', '7.1.2');
    xhr.setRequestHeader('User-Agent', 'Parse Android SDK API Level 25');
    xhr.setRequestHeader('X-Parse-Installation-Id', 'fecd381e-921e-4642-ab65-dd1a789d5cb8');
    xhr.setRequestHeader('X-Parse-Client-Key', '4Z29DJvRGdVnB5dcTvDTTG01fbkITxvcPCPOt21M');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Accept-Encoding', 'gzip, deflate, br');
    xhr.setRequestHeader('Connection', 'close');

    xhr.onload = function () {
        if (xhr.status >= 200 && xhr.status < 300) {
            console.log(xhr.responseText)
            callback(true, JSON.parse(xhr.responseText))
        } else {
            callback(false, xhr.statusText);
        }
    };

    xhr.onerror = function () {
        callback(false, xhr.statusText);
    };

    xhr.send(JSON.stringify({}));

}

function safeSet(rates,id, date, currency, name, callback){
        // Create a new XMLHttpRequest object
    const xhr = new XMLHttpRequest();

    // Set up the request
    xhr.open('POST', 'https://splid.herokuapp.com/parse/classes/GroupInfo', true);

    // Set request headers
    xhr.setRequestHeader('Host', 'splid.herokuapp.com');
    xhr.setRequestHeader('X-Parse-Application-Id', 'AKCaB0FCF0NIigWjxcDBpDYh7q6eN7gYfKxk5QBN');
    xhr.setRequestHeader('X-Parse-App-Build-Version', '142002');
    xhr.setRequestHeader('X-Parse-App-Display-Version', '1.4.2');
    xhr.setRequestHeader('X-Parse-Os-Version', '7.1.2');
    xhr.setRequestHeader('User-Agent', 'Parse Android SDK API Level 25');
    xhr.setRequestHeader('X-Parse-Installation-Id', 'fecd381e-921e-4642-ab65-dd1a789d5cb8');
    xhr.setRequestHeader('X-Parse-Client-Key', '4Z29DJvRGdVnB5dcTvDTTG01fbkITxvcPCPOt21M');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Accept-Encoding', 'gzip, deflate, br');
    xhr.setRequestHeader('Connection', 'close');

    xhr.onload = function () {
        if (xhr.status >= 200 && xhr.status < 300) {
            console.log("!!!!!!!!!!!!!!",xhr.responseText)
            callback(true, JSON.parse(xhr.responseText))
        } else {
            callback(false, xhr.statusText);
        }
    };

    xhr.onerror = function () {
        callback(false, xhr.statusText);
    };

    xhr.send(JSON.stringify({"creatorExperiments":{"__op":"Delete"},"currencyRates":rates,"group":{"__type":"Pointer","className":"_User","objectId":id},"wallpaperID":{"__op":"Delete"},"createdGlobally":{"__type":"Date","iso":date},"GlobalId":id,"defaultCurrencyCode":currency,"name":name}));

}
