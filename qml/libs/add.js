function randomString(length) {
  const characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
  let randomString = '';

  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    randomString += characters.charAt(randomIndex);
  }

  return randomString;
}

function genId(){
  return randomString(8)+"-"+randomString(4)+"-"+randomString(4)+"-"+randomString(4)+"-"+randomString(12)
}

function member(lst, callback){
    console.log("adding member")
        // Create a new XMLHttpRequest object
    const xhr = new XMLHttpRequest();

    // Set up the request
    xhr.open('POST', 'https://splid.herokuapp.com/parse/batch', true);

    // Set request headers
    xhr.setRequestHeader('Host', 'splid.herokuapp.com');
    xhr.setRequestHeader('X-Parse-Application-Id', 'AKCaB0FCF0NIigWjxcDBpDYh7q6eN7gYfKxk5QBN');
    xhr.setRequestHeader('X-Parse-App-Build-Version', '142002');
    xhr.setRequestHeader('X-Parse-App-Display-Version', '1.4.2');
    xhr.setRequestHeader('X-Parse-Os-Version', '7.1.2');
    xhr.setRequestHeader('User-Agent', 'Parse Android SDK API Level 25');
    xhr.setRequestHeader('X-Parse-Installation-Id', 'fecd381e-921e-4642-ab65-dd1a789d5cb8');
    xhr.setRequestHeader('X-Parse-Client-Key', '4Z29DJvRGdVnB5dcTvDTTG01fbkITxvcPCPOt21M');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Accept-Encoding', 'gzip, deflate, br');
    xhr.setRequestHeader('Connection', 'close');

    xhr.onload = function () {
        if (xhr.status >= 200 && xhr.status < 300) {
            console.log("!!!!!!!!!!!!!!",xhr.responseText)
            callback(true, JSON.parse(xhr.responseText))
        } else {
            callback(false, xhr.statusText);
        }
    };

    xhr.onerror = function () {
        callback(false, xhr.statusText);
    };
    console.log("adding...", lst)
    xhr.send(JSON.stringify({"requests":lst}));

}
function person(names, gid){
    console.log("adding person")
    names = names.replace(" ;", ";").replace("; ",";")
    var memberLst = []
    //add each person separated by ; to the appending member list which will be sent to the api
    console.log("LST", JSON.stringify(names.split(";")), "PURE", names)
    names.split(";").forEach(function(name){
        if(name.length == 0)return;
        memberLst.push({"method":"POST","path":"\/parse\/classes\/Person","body":{"group":{"__type":"Pointer","className":"_User","objectId":gid},"createdGlobally":{"__type":"Date","iso":"2024-01-20T17:28:01.524Z"}, "initials":name.slice(0,1),"name":name, "GlobalId":genId(), "UpdateID":genId()}})
    });
    member(memberLst, function(status,resulting){
        console.log(resulting)
        if(status)PopupUtils.close(regDia)

    })
}

function payment(id, transaction, amount, consumers, title, currency, mainPayer, secondaries, cat, callback) {
    if(cat == "unknown"){
        cat = {"__op":"Delete"}
    }else{
        cat = {"originalName":i18n.tr(cat),"type":cat}
    }
    console.log("CAT", JSON.stringify(cat))
    // Create a new XMLHttpRequest object
    const xhr = new XMLHttpRequest();

    // Set up the request
    xhr.open('POST', 'https://splid.herokuapp.com/parse/classes/Entry', true);

    // Set request headers
    xhr.setRequestHeader('Host', 'splid.herokuapp.com');
    xhr.setRequestHeader('X-Parse-Application-Id', 'AKCaB0FCF0NIigWjxcDBpDYh7q6eN7gYfKxk5QBN');
    xhr.setRequestHeader('X-Parse-App-Build-Version', '142002');
    xhr.setRequestHeader('X-Parse-App-Display-Version', '1.4.2');
    xhr.setRequestHeader('X-Parse-Os-Version', '7.1.2');
    xhr.setRequestHeader('User-Agent', 'Parse Android SDK API Level 25');
    xhr.setRequestHeader('X-Parse-Installation-Id', 'fecd381e-921e-4642-ab65-dd1a789d5cb8');
    xhr.setRequestHeader('X-Parse-Client-Key', '4Z29DJvRGdVnB5dcTvDTTG01fbkITxvcPCPOt21M');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('Accept-Encoding', 'gzip, deflate, br');
    xhr.setRequestHeader('Connection', 'close');

    xhr.onload = function () {
        if (xhr.status >= 200 && xhr.status < 300) {
            callback(true, JSON.parse(xhr.responseText))
        } else {
            callback(false, xhr.statusText);
        }
    };

    xhr.onerror = function () {
        callback(false, xhr.statusText);
    };
    let pmt = {"group":{"__type":"Pointer","className":"_User","objectId":id},"createdGlobally":{"__type":"Date","iso":"2024-01-20T17:30:17.101Z"},"date":{"__op":"Delete"},"UpdateID":genId(),"UpdateInstallationID":genId(),"isPayment":transaction,"items":[{"AM":amount,"P":{"PT":0,"P":consumers}}],"category":cat,"GlobalId":genId(),"currencyCode":currency,"primaryPayer":mainPayer}
    console.log("transmit payment", pmt)
    if(secondaries != {})pmt["secondaryPayers"] = secondaries
    //if external payment: allow title
    if(!(transaction))pmt["title"] = title
    xhr.send(JSON.stringify(pmt));
}

